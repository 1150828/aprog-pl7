/*
 * PL7 EX 4
 */
package aprog.pl7;

import java.util.Scanner;

public class Ex4 {
    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
         System.out.print("Introduza o tamanho do vetor: ");
         int n = ler.nextInt();
         int array [] = new int[n];
         int i;
         for(i=0;i<n;i++){
             System.out.print("Introduza um número inteiro para a posição " + i + " do vetor: ");
             int num = ler.nextInt();
             array [i] = num;
         }        
         int [] novovet = inverter(n,array);
         rotDireita(n, novovet);
    }
    
    public static int[] inverter(int size, int [] array){
        if(size > 0){
            for(int i=0; i<size/2;i++){
                int temp = array[i];
                array[i] = array [size -i -1];
                array[size -i-1] = temp;
            }
            System.out.println("\nVetor invertido:");
            for (int i = 0; i < size; i++) {
                System.out.print(array[i] + " ");
            }
            System.out.println("");
        }
            return(array);
    }
    
    public static void rotDireita(int size, int [] array){
       if(size>0){
        int temp = array [size-1];
        for(int i=size-2;i>=0;i--){
            array[i+1]=array[i];
        }
        array[0]=temp;
        System.out.println("\nVetor rodado para a direita:");
        for(int i=0; i<size; i++){
            System.out.print(array[i] + " ");
        }
        System.out.println("");
      }
    }
}