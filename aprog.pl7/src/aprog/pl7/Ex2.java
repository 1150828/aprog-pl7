/*
 * PL7 EX 2
 */
package aprog.pl7;

import java.util.Scanner;

public class Ex2 {
    public static Scanner in = new Scanner(System.in);
    
    public static int lerFunc(String[] nome, float[] vencimento){
        String nomeLido;
        float vencLido;
        int nElem = 0;
        do{
            System.out.print("Nome do funcionário na posição " + nElem + " (\"tt\" para terminar a leitura): ");
            nomeLido = in.next();
            
            if(nomeLido.equalsIgnoreCase("tt")) break;
            
            System.out.print("Vencimento de " + nomeLido + ": ");
            vencLido = in.nextFloat();
            
            nome[nElem] = nomeLido;
            vencimento[nElem] = vencLido;
            nElem++;
        }while(!nomeLido.equalsIgnoreCase("tt") && nElem<50);
        
        return nElem;
    }

    public static float mediaVenc(float[] vencimento, int nElem) {
        float media, soma = 0;
        for(int i = 0; i<nElem; i++){
            soma = soma + vencimento[i];
        }
        media = soma/nElem;
        return media;
    }
    
    public static void main(String[] args) {
        String nome[] = new String[50];
        float vencimento[] = new float[50];
        int nElem = lerFunc(nome, vencimento), funcAbaixo=0;
        float media = mediaVenc(vencimento, nElem), vencMax;
        
        System.out.println("\nFuncionários com vencimentos abaixo da média (" + media + "): ");
        for(int i = 0; i<nElem; i++){
            if(media>vencimento[i]){
                System.out.println("-"+nome[i]);
            }
        }
        
        System.out.print("\nFuncionários com vencimentos inferiores a: ");
        vencMax = in.nextFloat();
        
        for(int i = 0; i<nElem; i++){
            if(vencMax>vencimento[i]){
                funcAbaixo++;
            }
        }
        
        if(funcAbaixo!=0){
            double percentagem = (double) funcAbaixo/nElem*100;
            System.out.println("A percentagem de funcionários com vencimento abaixo de " + vencMax + " é " + percentagem + "%");
        } else {
            System.out.println("Nenhum funcionário tem um vencimento inferior ao inserido");
        }
    }
    
}
