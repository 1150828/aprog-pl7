/*
 * O algoritmo faz a leitura de 10 inteiros, ou seja, até o array estar completamente preenchido
 * de seguida verifica todo o array e soma todos os pares e faz contagem de quantos pares existem
 * e finalmente imprime a média dos pares.
 */
package aprog.pl7;

import java.util.Scanner;

public class Ex1 {
    
    public static int menorInt(int[] inteiros){
        int posMenorInt = 0;
        int menorInt = inteiros[0];
        for(int i = 1; i < inteiros.length; i++){
            if(menorInt > inteiros[i]){
                posMenorInt = i;
                menorInt = inteiros[i];
            }
        }
        return posMenorInt;
    }
    
    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
        int i, soma=0, pares=0, posMenorInt;
        int[] v = new int[10];

        for(i = 0; i < v.length; i++){
            System.out.print("Introduza o número para a posição " + i + ": ");
            v[i]=ler.nextInt();
        }
        for(i = 0; i < v.length; i++){
            if (v[i] % 2 == 0){
                soma = soma + v[i];
                pares++;
            }
        }
        if(pares!=0){
           System.out.println("A média dos pares introduzidos é "+((double)soma)/pares); 
        } else {
            System.out.println("Não foi introduzido nenhum número par.");
        }
        
        posMenorInt = menorInt(v);
        System.out.println("O menor número introduzido foi " + v[posMenorInt] + " que se encontra na posição " + posMenorInt);
    }
    
}
