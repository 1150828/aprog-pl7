/*
 * PL7 EX 6
 */
package aprog.pl7;

import java.util.Scanner;

public class Ex6 {
    
    public static Scanner ler = new Scanner(System.in);
    public static int maxVisitantes = 100;
    
    public static int insVisitante(String[] visitantes, int n){
        System.out.print("\nIntroduza o nome do visitante que deseja inserir na lista: ");
        visitantes[n]=ler.next();
        n++;
        return n;
    }
    
    public static void listVisitantes(String[] visitantes, int n){
        if(n==0){
            System.out.println("\nNão há nenhum visitante");
        } else {
            System.out.println("\nLista de visitantes:");
            for(int i = 0; i<n; i++){
                System.out.println(i + "-" + visitantes[i]);
            }
        }
    }
    
    public static void atuaVisitante(String[] visitantes, int n){
        int idAlt;
        do{
            System.out.print("\nNúmero do visitante que deseja alterar: ");
            idAlt=ler.nextInt();
        } while(idAlt<0 && idAlt>n);
        System.out.print("Novo nome para \"" + visitantes[idAlt] + "\": ");
        visitantes[idAlt]=ler.next();
    }
    
    public static int elimVisitante(String[] visitantes, int n){
        int idElm;
        do{
            System.out.print("\nNúmero do visitante que deseja eliminar: ");
            idElm=ler.nextInt();
        } while(idElm<0 && idElm>n);
        
        for (int i = idElm; i < n; i++){
            visitantes[i]=visitantes[i+1];
        }
        
        return --n;
    }

    public static void listLetra(String[] visitantes, int n) {
        int letra, cont=0;
        System.out.print("\nListar nomes começados por: ");
        letra=ler.next().charAt(0);
        
        for(int i=0; i<n; i++){
            if(letra==visitantes[i].charAt(0)){
                System.out.println("-" + visitantes[i]);
            }
        }
        
        if(cont==0) System.out.println("\nNão há nenhum nome começado por " + letra);
    }
    
    public static void listRepetidos(String[] visitantes, int n, int[] repetidos){
        int nRep = 0;
        boolean naLista = false;
        for(int i=0; i<n; i++){
            for(int j=0; j<n; j++){
                if(visitantes[i].equalsIgnoreCase(visitantes[j]) && i!=j){
                    for(int k=0; k<nRep; k++){
                        if(repetidos[k]==i){
                            naLista = true;
                        }
                    }
                    if(!naLista){
                        repetidos[nRep]=i;
                        nRep++;
                    }
                    naLista = false;
                }
            }
        }
        if(nRep==0){
            System.out.println("\nNão existem repetidos.");
        } else {
            System.out.println("\nLista de nomes repetidos:");
            for(int l=0; l<nRep; l++){
                System.out.println("-"+visitantes[repetidos[l]]);
            }
        }
    }
    
    public static void main(String[] args) {
        String[] visitantes = new String[maxVisitantes];
        int[] repetidos = new int[maxVisitantes];
        int n = 0;
        
        char op;
        do {
            System.out.print("\n1-Inserir um visitante\n2-Listar visitantes\n3-Atualizar um nome\n4-Eliminar um nome\n5-Listar nomes começados por uma letra\n6-Listar nomes repetidos\n0-Terminar\n\nEscolha uma opção: ");
            op = ler.next().charAt(0);
            switch (op) {
                case '1': 
                    if(n!=maxVisitantes-1){
                        n=insVisitante(visitantes, n);
                    } else {
                        System.out.println("\nJá introduziu o número máximo de visitantes.");
                    }
                    break;
                case '2': 
                    listVisitantes(visitantes, n);
                    break;
                case '3':
                    atuaVisitante(visitantes, n);
                    break;
                case '4':
                    n=elimVisitante(visitantes, n);
                    break;
                case '5':
                    listLetra(visitantes, n);
                    break;
                case '6':
                    listRepetidos(visitantes, n, repetidos);
                    break;
                case '0':
                    System.out.println("[PROGRAMA TERMINADO]");
                    break;
                default:
                    System.out.println("Opção inválida!");
            }
        } while (op != '0');
    }
    
}
