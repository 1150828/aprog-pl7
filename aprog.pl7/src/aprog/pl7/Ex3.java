/*
 * PL7 EX 3
 * Este algoritmo inicialmente apresenta um menu em que pode ser escolhida a opção de adicionar nomes,
 * os nomes introduzidos são adicionados a um array e depois quando forem introduzidos 100 nomes ou
 * o utilizador terminar a leitura com a palavra "fim" o programa volta ao menu, onde pode listar todos
 * os nomes ou remover o primeiro nome igual ao nome introduzido pelo utilizador, existe ainda
 * uma ultima opção para encerrar o programa.
 */
package aprog.pl7;

import java.util.Scanner;

public class Ex3 {
    public static Scanner ler = new Scanner(System.in);
    
    public static void main(String[] args) {
        int n=0;
        String nomes[] = new String[100];
        String m="\n1-Ler Nomes\n2-Remover Nome\n3-Listar Nomes\n4-Terminar\n\nEscolha uma opção: ";
        char op;
        do {
            System.out.print(m);
            op = ler.next().charAt(0);
            switch (op) {
                case '1': 
                    n = lerNomes(nomes);
                    break;
                case '2': 
                    System.out.print("\nNome a remover: ");
                    String nome = ler.next();
                    n = remover(nomes,nome,n);
                    break;
                case '3':
                    listar(nomes, n);
                    break;
                case '4':
                    break;
                default:
                    System.out.println("Opção inválida!!");
            }
        } while (op != '4');
    }
    
    private static int lerNomes(String[] vec) {
        int n = 0;
        String nomeIns;
        do{
            System.out.print("Introduza um nome (Digite FIM para terminar): ");
            nomeIns = ler.next();
            if(nomeIns.equalsIgnoreCase("fim")) break;
            vec[n] = nomeIns;
            n++;
        }while(!nomeIns.equalsIgnoreCase("fim") && n<100);
        return n;
        // Lê uma sequência de nomes terminada com a palavra FIM.
        // Armazena os nomes em vec e retorna o número desses nomes.
    }
    private static int listar(String[] vec, int n) {
        System.out.println("\nLista dos Nomes:");
        for(int i = 0; n>i; i++){
            System.out.println(vec[i]);
        }
        return 0;
        // Apresenta os primeiros n elementos de vec
    }
    private static int remover(String[] nomes, String nome, int n) {
        int i=0;
        while (i<n && !nomes[i].equalsIgnoreCase(nome)) {
            i++;
        }
        if(i==n){
            return n;
        } else {
            for (int j = i; j < n-1; j++){
                nomes[j]=nomes[j+1];
            }
            return --n;
        }
    }
}