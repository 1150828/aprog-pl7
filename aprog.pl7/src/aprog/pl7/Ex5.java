/*
 * PL7 EX 5 Completo
 */
package aprog.pl7;

import javax.swing.JOptionPane;

public class Ex5 {
    
    public static int lerNomes(String[] nomes, int n){
        String nomeInserido;
        do{
            nomeInserido = JOptionPane.showInputDialog(null, "Nome para a posição " + n + ": ");
            if(nomeInserido.equalsIgnoreCase("fim")) break;
            nomes[n]=nomeInserido;
            n++;
        }while(!nomeInserido.equalsIgnoreCase("fim") && n!=99);
        return n;
    }
    
    public static void mostrarListagem(String[] nomes, int n, String titulo){
        String mensagem = "";
        if(n!=0){
            for(int i=0; i<n; i++){
                if(i!=0) mensagem+="\n";
                mensagem+=nomes[i];
            }  
        } else {
            mensagem="Não há nenhum nome na lista.";
        }
        JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.PLAIN_MESSAGE);
    }
    
    public static String apelido(String nome){
        String apelido = nome.substring(nome.lastIndexOf(" ")+1);
        return apelido;
    }
    
    public static int preencherVetorApelidosS(String[] nomes, int n, String[] apelidos){
        int apelidosS = 0;
        for(int i=0; i<n; i++){
            if(apelido(nomes[i]).charAt(0) == 's' || apelido(nomes[i]).charAt(0) == 'S'){
                apelidos[apelidosS] = nomes[i];
                apelidosS++;
            }
        }
        return apelidosS;
    }
    
    public static void main(String[] args) {
        int n = 0, nApelidosS;
        String[] nomes = new String[100];
        String[] apelidosS = new String[100];
        n = lerNomes(nomes, n);
        mostrarListagem(nomes, n, "Lista de nomes inseridos");
        nApelidosS = preencherVetorApelidosS(nomes, n, apelidosS);
        mostrarListagem(apelidosS, nApelidosS, "Lista de apelidos começados por S");
    }
    
}
